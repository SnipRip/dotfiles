# SnipRip dotfiles repo

This repo is ment to sync all my configs on various machines and provide certian degree of automation when I switch machines.

## Your average usage:

SSH -       git@gitlab.com:SnipRip/dotfiles.git

HTTPS -     https://SnipRip@gitlab.com/SnipRip/dotfiles.git


## Usage for new machines provisioning:

Usage is as follows:
- download installation script
- run script with super user with switch to preserv your user as owner of created files and folders
- (optionaly) you can download installation script and modify to your needs

Quick rerference for command line installation:

```
wget https://gitlab.com/SnipRip/dotfiles/raw/master/provision.sh

sudo -u (your user name here) bash -x provision.sh
```

## TODO

- [ ] Update provision.sh to auto manage password
- [ ] Mangage sudo usage in provisioning
- [ ] Set separate package list for different distros
