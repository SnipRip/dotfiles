#!/bin/bash

if hash apt 2>/dev/null; then
    export PKG_INSTALLER=apt
else
    export PKG_INSTALLER=yum
fi

if ! hash ansible-playbook 2>/dev/null; then
    if hash apt; then
        sudo apt install software-properties-common -y
        echo -ne '\n' | sudo apt-add-repository ppa:ansible/ansible
        sudo apt update
    fi
    sudo $PKG_INSTALLER install -y ansible
fi

if [ ! -f $HOME/provision.yml ]; then
    wget https://gitlab.com/SnipRip/dotfiles/raw/master/provision.yml --no-check-certificate
    wget https://gitlab.com/SnipRip/dotfiles/raw/master/workapps --no-check-certificate
fi

sudo $PKG_INSTALLER update -y
ansible-playbook -v -i , $HOME/provision.yml --ask-become-pass | sed 's/\\n/\n/g'
