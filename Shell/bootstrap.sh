#!/bin/bash

cd ~/Downloads/dotfiles

cat Shell/bashrc >> ~/.bashrc

rm -rf .git*
rsync -uv ~/Downloads/dotfiles/{Vim,Tmux}/.* ~/

mkdir -p $HOME/devops/{git,notebooks,vagrant}

mkdir -p $HOME/.local/share/fonts
if [ ! -f $HOME/.local/share/fonts/Droid* ]; then
    rsync -uv ~/Downloads/dotfiles/Vim/Droid* $HOME/.local/share/fonts/
    fc-cache -f -v
fi

if [ ! -d ~/.vim/bundle/Vundle.vim ]; then
    echo "Installing Vundle..."
    git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
fi
echo -ne '\n' | vim +PluginInstall +qall
