" ==== LOAD PLUGINS ====

set nocompatible
filetype off


set runtimepath+=~/.vim/bundle/Vundle.vim

call vundle#begin()

" Init
Plugin 'gmarik/Vundle.vim'

" Visual
Plugin 'altercation/vim-colors-solarized'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'freeo/vim-kalisi' "for gvim
Plugin 'mhinz/vim-startify'
Plugin 'davb5/wombat256dave'
Plugin 'tyrannicaltoucan/vim-quantum'
Plugin 'sherifkandeel/vim-colors'
Plugin 'raphamorim/lucario'
Plugin 'juanedi/predawn.vim'
Plugin 'marcopaganini/termschool-vim-theme'
Plugin 'joshdick/onedark.vim'
Plugin 'widatama/vim-phoenix'
Plugin 'jordwalke/flatlandia'

" Utils
Plugin 'klen/python-mode'
Plugin 'kien/ctrlp.vim'
Plugin 'vim-syntastic/syntastic'
Plugin 'airblade/vim-gitgutter'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-surround'
Plugin 'davidhalter/jedi-vim'
Plugin 'Yggdroot/indentLine'
Plugin 'mbbill/undotree'
Plugin 'majutsushi/tagbar'
Plugin 'scrooloose/nerdcommenter'
Plugin 'severin-lemaignan/vim-minimap'
Plugin 'junegunn/limelight.vim'
Plugin 'junegunn/goyo.vim'
Plugin 'pearofducks/ansible-vim'

" NerdTree
Plugin 'scrooloose/nerdtree'
"Plugin 'Xuyuanp/nerdtree-git-plugin'
Plugin 'ryanoasis/vim-devicons' "fonts install needed!
Plugin 'tiagofumo/vim-nerdtree-syntax-highlight'

" Snipmate
Plugin 'sirver/ultisnips'
Plugin 'honza/vim-snippets'

call vundle#end()

filetype plugin indent on


" ==== NATIVE SETTINGS ====


autocmd! bufwritepost .vimrc source %           " automatic reloading of .vimrc
set pastetoggle=<F2>                            " Better copy & paste
set clipboard=unnamed
set bs=2                                        " make backspace behave like normal again
set mouse=a                                     " enable mouse
let mapleader = ","                             " Rebind <Leader> key
noremap <C-n> :nohl<CR>                         " Rebind nohl in all modes
vnoremap <C-n> :nohl<CR>
inoremap <C-n> :nohl<CR>
inoremap <F1> <ESC>                             " remove F1 help
nnoremap <F1> <ESC>
vnoremap <F1> <ESC>
noremap <C-Z> :update<CR>                       " Quicksave command in all modes
vnoremap <C-Z> <C-C>:update<CR>
inoremap <C-Z> <C-O>:update<CR>
noremap <Leader>e :quit<CR>                     " Quit current window
noremap <Leader>E :qa!<CR>                      " Quit all windows
map <c-j> <c-w>j                                " Rebind movment around windows to save keystrokes
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h
map <Leader>n <esc>:tabprevious<CR>             " easier moving between tabs
map <Leader>m <esc>:tabnext<CR>
vnoremap < <gv  " better indentation            " easier moving of code blocks
vnoremap > >gv  " better indentation
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red |" Show whitespace
au InsertLeave * match ExtraWhitespace /\s\+$/
" Disable syntax highlight for files larger than 50 MB
autocmd BufWinEnter * if line2byte(line("$") + 1) > 50000000 | syntax clear | endif
filetype plugin indent on                       " Enable syntax highlighting
syntax on
set number                                      " show line numbers
set tw=79                                       " width of document (used by gd)
set nowrap                                      " don't automatically wrap on load
set fo-=t                                       " don't automatically wrap text when typing
set colorcolumn=80                              " set column where you should stop
highlight ColorColumn ctermbg=233
set history=700                                 " Useful settings
set undolevels=700
set cursorline
set scrolloff=3
set tabstop=4                                   " use spaces set for python
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab
set hlsearch                                    " Make search case insensitive
set incsearch
set ignorecase
set smartcase
set nobackup                                    " Disable stupid backup and swap files - they trigger too many events
set nowritebackup
set noswapfile
au FocusLost * :set number                      " set numbers in left side
au FocusGained * :set relativenumber
autocmd InsertEnter * :set number
autocmd InsertLeave * :set relativenumber
set relativenumber
set number
set foldlevel=99                                " start vim with everything unfolded
cmap w!! w !sudo tee % >/dev/null               " allows to use sudo when saving
set lazyredraw                                  " redraw only when we need to.


" ==== PLUGINS CONFIG ====


" Colorscheme
set t_Co=256
set background=dark
"let g:solarized_termcolors=256
colorscheme wombat256dave
hi Normal guibg=NONE ctermbg=NONE             | " for transparent background settings


" python-mode
map <Leader>g :call RopeGotoDefinition()<CR>
let ropevim_enable_shortcuts = 1
let g:pymode_rope_goto_def_newwin = "vnew"
let g:pymode_rope_extended_complete = 1
let g:pymode_breakpoint = 0
let g:pymode_syntax = 1
let g:pymode_virtualenv = 1


" ctrlp
let g:ctrlp_max_height = 30
set wildignore+=*.pyc
set wildignore+=*build/*
set wildignore+=*dist/*
set wildignore+=*.egg-info/*
set wildignore+=*/coverage/*


" nerdtree
nmap <leader>t :NERDTree<CR>
let NERDTreeIgnore=['\.pyc$','\~$']
let NERDTreeShowHidden=1
let NERDTreeQuitOnOpen = 0
let NERDTreeChDirMode=2
let NERDTreeShowHidden=1
"let NERDTreeShowLineNumbers=1


" nerdtree devicons
set guifont=Droid\ Sans\ Mono\ for\ Powerline\ Nerd\ Font\ Complete\ 11
set encoding=utf-8


" nerdtree highlight
let g:NERDTreeFileExtensionHighlightFullName = 1
let g:NERDTreeExactMatchHighlightFullName = 1
let g:NERDTreePatternMatchHighlightFullName = 1
let g:WebDevIconsUnicodeDecorateFolderNodes = 1 "enables folder icons
let g:DevIconsEnableFoldersOpenClose = 0
let g:WebDevIconsNerdTreeAfterGlyphPadding = ' ' "size of space around icons
let g:webdevicons_conceal_nerdtree_brackets = 1 "brackes around icons
let g:WebDevIconsUnicodeGlyphDoubleWidth = 0
let g:NERDTreeDirArrowExpandable=""
let g:NERDTreeDirArrowCollapsible=""


" syntasctic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
set statusline+=%{fugitive#statusline()}
let g:syntasctic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1


" airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
nmap <leader>, :bnext<CR>
nmap <leader>. :bprevious<CR>
let g:airline#extensions#tabline#buffer_idx_mode = 1
nmap <leader>1 <Plug>AirlineSelectTab1
nmap <leader>2 <Plug>AirlineSelectTab2
nmap <leader>3 <Plug>AirlineSelectTab3
nmap <leader>4 <Plug>AirlineSelectTab4
nmap <leader>5 <Plug>AirlineSelectTab5
nmap <leader>6 <Plug>AirlineSelectTab6
nmap <leader>7 <Plug>AirlineSelectTab7
nmap <leader>8 <Plug>AirlineSelectTab8
nmap <leader>9 <Plug>AirlineSelectTab9


" Snippets
let g:UltiSnipsExpandTrigger="<Tab>"
let g:UltiSnipsJumpForwardTrigger="<Tab>"
let g:UltiSnipsJumpBackwardTrigger="<S-Tab>"


" gitgutter
hi clear SignColumn
let g:airline#extensions#hunks#non_zero_only = 1
set updatetime=100


" undertree
nnoremap <F5> :UndotreeToggle<cr>
let g:undotree_SetFocusWhenToggle=1


" tagbar
nmap <F8> :TagbarToggle<CR>
let g:tagbar_autofocus=1
let g:tagbar_expand=1
let g:tagbar_foldlevel=2
let g:tagbar_autoshowtag=1


" indentline
let g:indentLine_char = '┊'
let g:indentLine_showFirstIndentLevel = 1
let g:indentLine_first_char = '┊'
"let g:indentLine_leadingSpaceEnabled = 1
"let g:indentLine_leadingSpaceChar = '·'


" Goyo and Limelight
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!
let g:goyo_width=85
nmap <Leader>l :Goyo<CR>
